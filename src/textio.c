#include <stdbool.h>
#include "stivale.h"
#include "graphics.h"
#include "font.h"
#include "textio.h"
#include "panic.h"
#include "string.h"

#define PIXELAVAL(pos) ((struct pixel32 *)&FRAMEBUFFER[pos])->A
#define GETCHARLOC(coords) FB_LOCATE(coords.x, coords.y)


uint8_t font_padding = 2;
struct pixel32 font_color_fg = {
    .R = 0xFF,
    .G = 0xFF,
    .B = 0xFF
};
struct pixel32 font_color_bg = {
    .R = 0x00,
    .G = 0x00,
    .B = 0x00
};
struct coords textio_cursor;
enum pixstate default_pixstate = aval_from_out;


static void cursor_nextchar();

static inline void cursor_resetx() {
    textio_cursor.x = font_padding;
}
static inline void cursor_resety() {
    textio_cursor.y = font_padding;
}

void textio_init() {
    cursor_resetx();
    cursor_resety();
}

static inline void cursor_prevline() {
    cursor_resetx();
    textio_cursor.y -= font_padding + FONT_HEIGHT;
    // Find last character in line
    while (PIXELAVAL(GETCHARLOC(textio_cursor)) != aval_free) {
        cursor_nextchar();
    }
}
static inline void cursor_nextline() {
    cursor_resetx();
    textio_cursor.y += font_padding + FONT_HEIGHT;
    // Scroll
    if (textio_cursor.y + FONT_HEIGHT >= stivale->framebuffer_height) {
        memcpy(
               FRAMEBUFFER,
               FRAMEBUFFER + stivale->framebuffer_width * FONT_HEIGHT,
               stivale->framebuffer_width * (stivale->framebuffer_height - FONT_HEIGHT) * sizeof(struct pixel32)
              );
        cursor_prevline();
        memset(
               &FRAMEBUFFER[GETCHARLOC(textio_cursor)],
               0,
               stivale->framebuffer_width * (FONT_HEIGHT) * sizeof(struct pixel32)
              );
    }
}

static inline void cursor_prevchar() {
    textio_cursor.x -= FONT_WIDTH + font_padding;
    // Go to previous line if required
    if (textio_cursor.x > stivale->framebuffer_width) {
        cursor_prevline();
    }
}
static inline void cursor_nextchar() {
    textio_cursor.x += FONT_WIDTH + font_padding;
    // Go to next line if required
    if (textio_cursor.x + FONT_WIDTH > stivale->framebuffer_width) {
        cursor_nextline();
    }
}

static inline void cursor_lerase() {
    cursor_prevchar();
    putchar_simple(' ');
    PIXELAVAL(GETCHARLOC(textio_cursor)) = aval_free;
}
static inline void cursor_rerase() {
    cursor_nextchar();
    putchar_simple(' ');
    PIXELAVAL(GETCHARLOC(textio_cursor)) = aval_free;
    cursor_prevchar();
}

inline bool putchar_simple(const char character) {
    const bool *bitmap = font_get_character(character);
    uint32_t thischarloc = GETCHARLOC(textio_cursor);
    // Check if nextchar is in bounds
    PANIC_IF(thischarloc >= FB_LOCATE(stivale->framebuffer_width, stivale->framebuffer_height));
    // Check if character is special
    if (bitmap == fontBitmapUnk) {
        switch(character) {
            case '\n': cursor_nextline(); return false;
            case '\r': cursor_resetx(); return false;
            case '\b': cursor_lerase(); return false;
        }
    }
    // Render
    int x = 0;
    int y = 0;
    for (const bool *thispixel = bitmap; ; thispixel++) {
        if (x == FONT_WIDTH) {
            x = 0;
            y++;
        } if (y == FONT_HEIGHT) {
            break;
        }
        FRAMEBUFFER[FB_LOCATE(textio_cursor.x + x, textio_cursor.y + y)] = *thispixel?font_color_fg:font_color_bg;
        x++;
    }
    PIXELAVAL(thischarloc) = default_pixstate;
    return true;
}

inline void putchar(const char character) {
    if (!putchar_simple(character)) {
        return;
    }
    cursor_nextchar();
}

void print(const char *string) {
    if (!string) {
        string = "(null)";
    }
    for (const char *character = string; *character != '\0'; character++) {
        putchar(*character);
    }
}

bool lastchar_has_pixstate(enum pixstate expected) {
    cursor_prevchar();
    bool fres = PIXELAVAL(GETCHARLOC((textio_cursor))) == expected;
    cursor_nextchar();
    return fres;
}
