#include <cstddef>
#include <cstdint>
#include "heap.h"


void *operator new(size_t len) {
    return malloc(len);
}
void *operator new[](size_t len) {
    return malloc(len);
}
void operator delete(void *ptr, unsigned long) {
    free(ptr);
}
void operator delete[](void *ptr, unsigned long) {
    free(ptr);
}
void operator delete[](void *ptr) {
    free(ptr);
}
