#include "stl.hpp"
#include "textio.hpp"
#include "textio.h"
#include "stdio.h"
#include "keyboard.h"
#include "idle.h"


std::string readin(const char delim, const char forbidchar, const std::string::size_type max_size) {
    std::string fres;
    auto backup_default_pixstate = default_pixstate;
    default_pixstate = aval_from_in;

    while (1) {
        char inchar;
        // Wait for input
        while (!(inchar = kb_getchar())) {idle();}
        // Check if character is forbidden
        if (inchar == forbidchar) continue;
        // Check if delimiter was reached
        if (inchar == delim) break;
        // Check if character removal was requested
        if (inchar == '\b') {
            // Check if removal is possible
            if ((not lastchar_has_pixstate(aval_from_in)) or fres.empty()) {
                continue;
            }
            fres.pop_back();
        } else {
            // Check if max size was reached
            if (max_size and fres.size() == max_size) continue;
            // Append character to buffer
            fres.push_back(inchar);
        }
        // Append character to output
        putchar(inchar);
    }

    if (delim == '\n') {
        putchar('\n');
    }

    default_pixstate = backup_default_pixstate;
    return fres;
}

void print(const std::string& str) {
    print(str.c_str());
}
