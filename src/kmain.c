#include "stivale.h"
#include "panic.h"
#include "textio.h"
#include "stdio.h"
#include "interrupts.h"
#include "keyboard.h"
#include "heap.h"
#include "string.h"
#include "idle.h"

void kmainpp();


[[noreturn]]
void kmain(struct stivale_struct* strct) {
    // Set doublefault handler
    register_interrupt_handler(0x08, (void*)(cpu_exception), 0, INTERRUPT_GATE);
    // Initiliase environemt
    init_from_stivale(strct);
    textio_init();
    kb_init();
    idt_init();
    kb_enable();
    // Switch to C++ code
    kmainpp();
}
