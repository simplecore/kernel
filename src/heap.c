#include <stdbool.h>
#include "stivale.h"
#include "heap.h"
#include "textio.h"
#include "panic.h"

static uint64_t mem_used = 0;

struct memchunk_free heap = { .next = 0, .size = 0 };

inline static uint64_t size_align(uint64_t num) {
    return ((num + sizeof(struct memchunk_free) - 1) / sizeof(struct memchunk_free)) * sizeof(struct memchunk_free);
}

void heap_init() {
    if (heap.next) return;
    heap.next = (struct memchunk_free *)higherhalf.base;
    heap.next->size = higherhalf.length;
    heap.next->next = 0;
}

void *heap_alloc(uint64_t *size) {
    // Make sure heap is initialised
    if (!heap.next) heap_init();

    // Allocations can't be smaller than this
    if (*size < sizeof(struct memchunk_free))
        *size = sizeof(struct memchunk_free);

    // Force-align size
    *size = size_align(*size);
    // Find first free space
    struct memchunk_free *nextmatch = heap.next, *baknext = &heap;

    // Get a chunk which fits this size
    while (nextmatch && nextmatch->size < *size) {
        baknext = nextmatch;
        nextmatch = nextmatch->next;
    }

    // If no chunk fits
    if (!nextmatch || nextmatch->size < *size) {
        panic("Out of heap memory!");
    }

    // Calculate the remaining size of the chunk
    uintptr_t new_size = nextmatch->size - *size;

    // If there is anything left, place it back in the list
    if (new_size >= sizeof(struct memchunk_free)) {
        struct memchunk_free *bak = baknext->next->next;
        baknext->next = (struct memchunk_free *)((uint64_t)nextmatch + *size);
        baknext->next->next = bak;
        baknext->next->size = new_size;
    } else {
        *size = nextmatch->size;
        baknext->next = nextmatch->next;
    }

    // Increase counter
    mem_used += *size;

    // Return allocated memory
    return nextmatch;
}

void *malloc(uint64_t size) {
    // Add space for size
    size += sizeof(uintptr_t);

    uint64_t *mem = (uint64_t *)heap_alloc(&size);

    if (!mem)
        return (void*)0;

    mem[0] = size;
    return mem + 1;
}

void heap_dealloc(void *memptr, uint64_t size) {
    // Convert this back into a chunk
    struct memchunk_free *chunk = (struct memchunk_free *)memptr;

    // Reinsert the chunk into the freelist
    chunk->size = size;
    chunk->next = heap.next;
    heap.next = chunk;

    // Decrease counter
    mem_used -= size;
}

void free(void *memptr) {
    if (!memptr)
        return;
    uint64_t *mem = (uint64_t *)memptr - 1;
    heap_dealloc(mem, mem[0]);
}

uint64_t get_mem_used() {
    return mem_used;
}
uint64_t get_mem_total() {
    return higherhalf.length;
}
uint64_t get_mem_free() {
    return get_mem_total() - get_mem_used();
}
