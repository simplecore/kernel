#include <stdbool.h>
#include "stivale.h"
#include "panic.bmp.h" // Must be 800x600x24
#include "graphics.h"
#include "textio.h"
#include "font.h"


static bool locked = false;


static inline struct pixel32 pix24topix32(struct pixel24 pixel) {
    struct pixel32 res = {
        .R = pixel.R,
        .G = pixel.G,
        .B = pixel.B,
    };
    return res;
}

void panic(const char *message) {
    if (locked) while(1) {asm("hlt");}
    locked = true;
    uint16_t x = 0;
    uint16_t y = 600;
    struct pixel32 *fbptr = (struct pixel32*)FRAMEBUFFER;
    struct pixel32 *fbendptr = (struct pixel32*)(FRAMEBUFFER + stivale->framebuffer_width * stivale->framebuffer_height);
    struct pixel24 *imgptr = (struct pixel24 *)(panic_bmp + 0x7a);
    // Draw loop
    while (1) {
        // Draw pixel
        fbptr[y * stivale->framebuffer_width + x] = pix24topix32(*(imgptr));
        // Next pixel
        if (++x == 800) {
            // Fill rest of the x axis
            while (x != stivale->framebuffer_width) {
                fbptr[y * stivale->framebuffer_width + x++] = pix24topix32(*imgptr);
            }
            // Next line
            x = 0;
            if (--y == 0) {
                break;
            }
        }
        // Next color value
        imgptr++;
    }
    // Print debug info
    textio_init();
    font_color_bg = pix24topix32(*imgptr);
    textio_cursor.y = 600 - (FONT_HEIGHT + font_padding) * 2;
    print("For more informations, look up this message:\n");
    print(message);
    // Freeze
    while(1) {asm("hlt");}
}

__attribute__((interrupt))
void cpu_exception(void *p) {
    (p);
    panic("CPU exception");
}
