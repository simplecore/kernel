#include "stl.hpp"
#include "textio.hpp"
#include "stdio.h"
#include "debug.hpp"
using namespace std;



extern "C"
[[noreturn]]
void kmainpp() {    
    print("TODO list implementation v1.0\n\n");

    vector<string> todo_list;
    while (true) {
        // Show prompt
        print("\n> ");

        // Get command
        auto cmd = readin();

        // Parse command
        if (cmd == "help" || cmd == "h" || cmd == "?") {
            print("Commands:\n"
                  " help: This overview\n"
                  " show: Show current list\n"
                  " add: Add an item\n"
                  " del: Delete an item\n"
                  " done: Mark item as done\n"
                  " tests: Run tests");
        } else if (cmd == "show" || cmd == "list") {
            print("Current list:\n");
            if (todo_list.empty()) {
                print(" (empty)\n");
            } else {
                size_t idx = 0;
                for (const auto& item : todo_list) {
                    print(string(" - ")+item+"\n");
                    idx++;
                }
            }
        } else if (cmd == "add") {
            print("Content: ");
            todo_list.push_back(readin());
        } else if (cmd == "del") {
            print("Number: ");
            auto idx = stoi(readin());
        } else if (cmd == "mem") {
            print_mem_usage();
        } else if (cmd == "tests") {
            run_tests();
        } else if (!cmd.empty()) {
            printf("Invalid command: %s\n", cmd.c_str());
        }
    }
}
