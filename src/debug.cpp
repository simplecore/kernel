#include "stl.hpp"
#include "textio.hpp"
#include "stivale.h"
#include "panic.h"
#include "textio.h"
#include "stdio.h"
#include "interrupts.h"
#include "keyboard.h"
#include "heap.h"
#include "string.h"
#include "idle.h"
#include "debug.hpp"



void run_tests(bool textio_fiddely) {
    std::string test = "Simple ";
    test.append("string test\n");
    print(test.c_str());

    print("Simple draw-anywhere test");
    auto cursor_bak = textio_cursor;
    textio_cursor.y += 5;
    print("Hello world!");
    textio_cursor = cursor_bak;
    putchar('\n');

    auto font_color_fg_bak = font_color_fg;
    auto font_color_bg_bak = font_color_bg;
    font_color_fg = {
        255,
        0,
        0
    };
    font_color_bg = {
        0,
        0,
        255
    };
    print("Simple font color test\n");
    font_color_fg = font_color_fg_bak;
    font_color_bg = font_color_bg_bak;

    putchar('\n');

    test = "String iteration with font color test\n";
    for (const auto& color : {&font_color_fg.R, &font_color_fg.G, &font_color_fg.B}) {
        auto colorbak = *color;
        for (const auto& character : test) {
            *color -= 255 / test.size();
            putchar(character);
        }
        *color = colorbak;
    }

    if (textio_fiddely) {
        test = "String iteration with draw-anywhere test";
        cursor_bak = textio_cursor;
        for (const auto& character : test) {
            putchar(character);
            textio_cursor.y++;
        }
        textio_cursor = cursor_bak;
        print("\n\n\n");

        print("Line drawing test\n");
        for (auto currx = 0; currx != stivale->framebuffer_width; currx++) {
            FRAMEBUFFER[FB_LOCATE(currx, textio_cursor.y - 1)] = {223, 134, 212};
        }
        print("\n\n");
    }

    print("Keyboard test\n");
    for (const auto& character : "Hello world, this is a simple string") {
        while (not kb_getchar()) {idle();}
        putchar(character);
    }
    print("\n\n");

    print("Done! Seems like it's all set and working.");
}

void print_mem_usage() {
    printf("Memory usage:\n"
           " - Used: %d KB\n"
           " - Free: %d KB\n"
           " - Total: %d KB\n",
           get_mem_used() / 1000,
           get_mem_free() / 1000,
           get_mem_total() / 1000
    );
}

void print_stivate() {
    print("struct stivale_struct stivale = {\n"); {
        #define STIVALE_DEBUG(key, type) printf("    ."#key" = %"#type",\n", stivale->key);
        STIVALE_DEBUG(cmdline, x);
        STIVALE_DEBUG(memory_map_addr, x);
        STIVALE_DEBUG(memory_map_entries, l);
        STIVALE_DEBUG(framebuffer_addr, x);
        STIVALE_DEBUG(framebuffer_pitch, l);
        STIVALE_DEBUG(framebuffer_width, l);
        STIVALE_DEBUG(framebuffer_height, l);
        STIVALE_DEBUG(framebuffer_bpp, l);
        STIVALE_DEBUG(rsdp, x);
        STIVALE_DEBUG(module_count, l);
        STIVALE_DEBUG(modules, x);
        STIVALE_DEBUG(epoch, l);
    } print("};\n");
}
