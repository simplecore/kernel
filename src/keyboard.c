/*
* Copyright (C) 2014  Arjun Sreedharan
* License: GPL version 2 or higher http://www.gnu.org/licenses/gpl.html
*/
#include "keyboard_map.h"
#include "panic.h"
#include "asmfn.h"
#include "textio.h"
#include "interrupts.h"

/* there are 25 lines each of 80 columns; each element takes 2 bytes */
#define KEYBOARD_DATA_PORT 0x60
#define KEYBOARD_STATUS_PORT 0x64

#define KB_BUFFER_SIZE 8192 // 8KB
#define ENTER_KEY_CODE 0x1C

static char kb_buffer[KB_BUFFER_SIZE] = {0};
static long kb_buffer_len = 0;


__attribute__((interrupt)) void keyboard_handler(void *p) {
    (void)p;

    unsigned char status;
    char keycode;

    /* write EOI */
    port_outb(0x20, 0x20);

    status = port_inb(KEYBOARD_STATUS_PORT);
    /* Lowest bit of status will be set if buffer is not empty */
    if (status & 0x01) {
        keycode = port_inb(KEYBOARD_DATA_PORT);

        if (keycode < 0)
            return;

        char thischar;
        if (keycode == ENTER_KEY_CODE) {
            thischar = '\n';
        } else {
            thischar = keyboard_map[(char) keycode];
        }

        if (thischar) {
            if (kb_buffer_len != KB_BUFFER_SIZE) {
                kb_buffer[kb_buffer_len++] = thischar;
            } else {
                panic("Keyboard buffer overflow");
            }
        }
    }
}

void kb_init() {
    // populate IDT entry of keyboard's interrupt
    register_interrupt_handler(0x21, keyboard_handler, 0, INTERRUPT_GATE);
}
void kb_enable() {
    // 0xFD is 11111101 - enables only IRQ1 (keyboard)
    port_outb(0x21 , 0xFD);
}

char kb_getchar() {
    // Iterate through buffer backwards
    for (long it = kb_buffer_len - 1; it != -1; it--) {
        if (kb_buffer[it] != 0) {
            char res = kb_buffer[it];
            kb_buffer[it] = 0;
            kb_buffer_len--;
            return res;
        }
    }
    return 0;
}
