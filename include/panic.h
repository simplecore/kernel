#ifdef __cplusplus
extern "C" {
#endif
[[noreturn]] void panic(const char *message);
[[noreturn]] void cpu_exception(void *);
#ifdef __cplusplus
}
#endif

#define PANIC_IF(expr) if (expr) panic("("#expr") == true")
