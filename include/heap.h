#include <stdbool.h>


struct memchunk_free {
    uint64_t size;
    struct memchunk_free *next;
};


#ifdef __cplusplus
extern "C" {
#endif
void heap_init();
void *malloc(uint64_t size);
void *heap_allocate(uint64_t *size);
void free(void *memptr);
void heap_deallocate(void *memptr, uint64_t size);

uint64_t get_mem_used();
uint64_t get_mem_total();
uint64_t get_mem_free();
#ifdef __cplusplus
}
#endif
