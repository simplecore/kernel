#ifdef __cplusplus
extern "C" {
#endif
void kb_init();
void kb_enable();
char kb_getchar();
#ifdef __cplusplus
}
#endif
