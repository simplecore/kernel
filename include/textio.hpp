#include "stl.hpp"

std::string readin(const char delim = '\n', const char forbidchar = 0, const std::string::size_type max_size = 0);
void print(const std::string& str);
