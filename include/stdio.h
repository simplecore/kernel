#ifdef __cplusplus
extern "C" {
#endif
int printf(const char *format, ...);
#ifdef __cplusplus
}
#endif
