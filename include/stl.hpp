#pragma once
extern "C"  {
#include "string.h"
}


// Classes
namespace std {
#define _stl_array_base(type) \
    public:\
    using iterator = type*;\
    using const_iterator = const iterator;\
    using size_type = size_t;


template<typename T>
class initializer_list {
    _stl_array_base(T)

protected:
    iterator arr;
    size_type len;

public:
    constexpr initializer_list(const T* a, size_type l) : arr(a), len(l) {

    }

    constexpr size_type
    size() const noexcept {
        return len;
    }

    constexpr const_iterator
    begin() const noexcept {
        return arr;
    }

    constexpr const_iterator
    end() const noexcept {
        return begin() + len;
    }
};


template<typename T, size_t len>
class array {
    _stl_array_base(T)

protected:
    T *obj;

public:
    array() {
        static_assert(len != 0, "Length must not be zero");
        obj = new T[len];
    }
    ~array() {
        delete []obj;
    }

    auto begin() {
        return obj;
    }
    auto end() {
        return obj+len;
    }

    auto get_len() const {
        return len;
    }
    auto get_obj() const {
        return obj;
    }

    auto operator [](size_t idx) {
        return obj[idx];
    }
    auto operator *() const {
        return *obj;
    }
    operator T*() {
        return obj;
    }
};


template<typename T>
class vector {
    _stl_array_base(T)

protected:
    size_type reallen = 0;
    size_type buflen = 10;
    const static size_type rahead = 16;
    T *buf;

    void realloc() {
        auto newbuf = new T[buflen];
        memcpy(newbuf, buf, reallen);
        delete []buf;
        buf = newbuf;
    }
    void fit() {
        if (reallen > buflen) {
            buflen = reallen + rahead;
            realloc();
        }
    }

public:
    vector() {
        buf = new T[buflen];
    }
    vector(const vector<T>& src) {
        reallen = src.size();
        buflen = reallen + rahead;
        buf = new char[buflen];
        memcpy(buf, src.buf, src.size());
    }
    vector(const initializer_list<T>& src) {
        for (const auto& elem : src) {
            push_back(elem);
        }
    }
    ~vector() {
        delete []buf;
    }

    auto size() const {
        return reallen;
    }
    auto length() const {
        return reallen;
    }
    auto empty() const {
        return size() == 0;
    }
    auto operator [](size_type index) const {
        return buf[index];
    }

    auto begin() const {
        return buf;
    }
    auto end() const {
        return buf+reallen;
    }
    auto& back() const {
        return buf[reallen - 1];
    }

    void push_back(const T& item) {
        reallen++;
        fit();
        buf[reallen-1] = item;
    }

    void erase(size_type pos, size_type size) {
        memcpy(buf + pos, buf + pos + size, reallen - pos + size);
        reallen -= pos + size;
    }
    void pop_back() {
        if (reallen) reallen--;
    }

    auto data() const {
        return buf;
    }

    void optimize() {
        buflen = reallen;
        realloc();
    }

    bool operator ==(const vector& other) {
        if (size() != other.size()) {
            return false;
        }
        for (auto idx = size() - 1; idx > 0; idx--) {
            if ((*this)[idx] != other[idx]) {
                return false;
            }
        }
        return true;
    }
};


class string : public vector<char> {
    _stl_array_base(char)

public:
    string() {}
    string(const char *src) {
        reallen = strlen(src);
        fit();
        strcpy(buf, src);
    }
    string(const char *src, size_type len) {
        reallen = len;
        fit();
        memcpy(buf, src, len);
    }

    string operator +(const string& src) {
        string cpy = *this;
        cpy.append(src);
        return cpy;
    }

    void append(const string& src) {
        reallen += src.size();
        fit();
        memcpy(buf + reallen - src.size(), src.buf, src.size());
    }

    const char *c_str() const {
        if (reallen < buflen && buf[reallen] != '\0') {
            // B00byedge, the chaddest — 8. May 2021 at 17:02
            // > don't do that
            auto rwthis = const_cast<string*>(this);
            rwthis->push_back('\0');
            rwthis->reallen--;
        }
        return buf;
    }

    bool operator ==(const char *other) {
        return reinterpret_cast<vector&>(*this) == string(other);
    }
};

#undef _stl_array_base
}

// Functions
namespace std {
template<typename intT = int>
auto stoi(const std::string& str) {
    intT fres = 0;
    // Iterate through string backwards
    for (const auto character : str) {
        fres += character - '0';
        fres *= 10;
    }
    fres /= 10;
    // Return result
    return fres;
}

template<typename intT>
string to_string(intT value) {
    char fres[22];
    int off = sizeof(fres);
    bool neg = false;
    if (value < 0) {
        neg = true;
        value = -value;
    }
    do {
        fres[--off] = (value%10)+'0';
        value /= 10;
    } while (value);
    if (neg) {
        fres[--off] = '-';
    }
    return string(fres+off, sizeof(fres)-off);
}
}
