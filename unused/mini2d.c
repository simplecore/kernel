#include "mini2d.h"

#define precision 500
#define anyabs(val) (val)<0?-(val):(val)


struct mini2d mini2d(mini2dpixel *buffer, int width, int height) {
    // Initialise basic data
    struct mini2d res = {
        .screen_begin = buffer,
        .screen_end = buffer + width * height,
        .screen_half = buffer + (width * height / 2) + (width / 2),
        .screen_height = height,
        .screen_width = width,
    };
    // Return resulting struct
    return res;
}

inline static struct coords_real resolvereal(struct mini2d *state, struct coords_2d *coords) {
    // Calculate
    struct coords_real res = {
        .x = (int)((float)state->screen_width / 200 * coords->x * 100),
        .y = (int)((float)state->screen_height / 200 * coords->y * 100),
    };
    return res;
}

int mini2d_drawpix(struct mini2d *state, struct coords_real *coords, mini2dpixel color) {
    // Get pixel pointer
    mini2dpixel *thispixel = &state->screen_half[state->screen_width * coords->y + coords->x];
    if (thispixel > state->screen_end ||
        thispixel < state->screen_begin) {
        return 0;
    }
    *thispixel = color;
    return 1;
}

void mini2d_drawline(struct mini2d *state, struct line *thisline) {
    // Calculate distance
    struct coords_2d distance = {
        .x = anyabs(thisline->start.x - thisline->end.x),
        .y = anyabs(thisline->start.y - thisline->end.y),
    };
    // Draw loop
    struct coords_2d thispos = thisline->start;
    while (1) {
        // Draw pixel
        struct coords_real coordsreal = resolvereal(state, &thispos);
        mini2d_drawpix(state, &coordsreal, thisline->color);
        // Next pixel
        thispos.x += distance.x / precision;
        thispos.y += distance.y / precision;
        if (thispos.x > thisline->end.x) {
            break;
        }
    }
}

void mini2d_drawrect(struct mini2d *state, struct rectangle *thisrect) {
    // Calculate distance
    struct coords_2d distance = {
        .x = anyabs(thisrect->bottomright.x - thisrect->topleft.x),
        .y = anyabs(thisrect->bottomright.y - thisrect->topleft.y),
    };
    // Draw loop
    struct coords_2d thispos = thisrect->topleft;
    while (1) {
        // Draw pixel
        struct coords_real coordsreal = resolvereal(state, &thispos);
        mini2d_drawpix(state, &coordsreal, thisrect->color);
        // Next pixel
        if (thispos.x <= thisrect->bottomright.x) {
            // Go right
            thispos.x += distance.x / precision;
        } else if (thispos.y >= thisrect->bottomright.y) {
            break;
        } else {
            // Go to next line
            thispos.x = thisrect->topleft.x;
            thispos.y += distance.y / precision;
        }
    }
}
